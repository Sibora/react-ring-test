module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "jsx": true
        },
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "globals": {
        "React": true,
        "ReactDOM": true,
        "VERSION": false,
        "DSx": false,
        "SuperModal": false,
        "API": false,
        "Mask": false,
        "Alert": false,
        "Tooltip": false,
        "require": false,
        "Scroller": false,
        "Spinner": false,
        "Highcharts": false,
        "widget": false,
    },
    "rules": {
        "indent": [
            "error",
            4
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "react/jsx-uses-vars": [2],
        "react/jsx-uses-react": "error",
    }
};