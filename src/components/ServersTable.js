import React, { Component } from "react";
//import testState from './testState.json'


export default class ServersTable extends Component {

    render() {
        let dotClass = 'height: "25px", width: "25px", backgroundColor: "#fff", borderRadius: "50%", display: "inline-block" '

        let servers = this.props.servers

        // to use testState for working faster 
        // let servers = testState.servers 

        function bytesToSize(bytes) {
            var sizes = ['B', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0) return '0 B';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        };

        return (
            <div className="container" style={{ marginTop: '20px' }}>
                <strong>SERVERS</strong>
                <table className="table table-striped table-dark">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Status</th>
                            <th scope="col">Zone</th>
                            <th scope="col">Hardware</th>
                            <th scope="col">Capacity</th>
                            <th scope="col">Roles</th>


                        </tr>
                    </thead>
                    <tbody>
                        {servers ? servers.map((server) =>
                            (
                                <tr>
                                    <td scope="col">{server.name}</td>
                                    <td scope="col">
                                        <span
                                            style={server.status == "OK" ?
                                                { color: "#21ac4d" }
                                                : { color: "#ef3340" }}>
                                            {server.status == "OK" ? "✔" : "✘"}
                                        </span>
                                    </td>
                                    <td scope="col">{server.zone}</td>
                                    <td scope="col">{server.server_type ? server.server_type : "Not Specified"}</td>
                                    <td scope="col">{bytesToSize(server.hdd_disk_size)}</td>
                                    <td scope="col">
                                        {server.roles.map((role) =>
                                            <button type="button" class="btn btn-primary" style={{ marginRight:"3px"}}>
                                                {role}
                                            </button>)}
                                    </td>
                                </tr>
                            )
                        )
                            : null
                        }


                    </tbody>
                </table>
            </div>
        )
    }
}
