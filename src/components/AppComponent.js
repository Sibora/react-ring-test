import React, { Component } from "react";
import ServersTable from './ServersTable'
import DisksTable from './DisksTable'


const servers_url = 'http://localhost:8888/api/v0.1/servers/';
const disks_url = 'http://localhost:8888/api/v0.1/disks/';

export default class AppComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    // fetch from api, convert to json and put server and disks data in state object
    componentDidMount() {

        Promise.all([
            fetch(servers_url)
                .then(response => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error('Something went wrong trying to fetch from ' + servers_url + '.');
                    }
                }),
            fetch(disks_url).
                then(response => {
                    if (response.ok) {
                        return response.json();
                    } else {
                        throw new Error('Something went wrong trying to fetch from ' + disks_url + '.');
                    }
                })
        ]).then((data) =>
            this.setState({ servers: data[0]._items, disks: data[1]._items }));

    }

    render() {
        
        const state = this.state
        console.log('state: ', state);


        return (
            <div>
                <div>
                    <ServersTable
                        servers = {state.servers}
                    />
                </div>
                <div>
                    <DisksTable
                        disks = {state.disks}
                        servers = {state.servers}
                    />
                </div>
            </div>
        )
    }
}
