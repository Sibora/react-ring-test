import React, { Component } from "react";
import testState from './testState.json'

export default class DisksTable extends Component {

    render() {
        // let disks = this.props.disks
        // let servers = this.props.servers

        // to use testState for working faster 
        let disks = testState.disks
        let servers = testState.servers

        return (
            <div className="container" style={{ marginTop: '20px' }}>
                <strong>DISKS</strong>
                <table className="table table-striped table-dark">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Server</th>
                            <th scope="col">Status</th>
                            <th scope="col">Usage</th>


                        </tr>
                    </thead>
                    <tbody>
                        {disks ? disks.map((disk) =>
                            (
                                <tr>
                                    <td scope="col">{disk.name}</td>

                                    {servers.map((server) =>
                                        server.id == disk.server ?
                                            <td scope="col">{server.name}</td>
                                            : null
                                    )}

                                    <td scope="col">
                                        <span
                                            style={disk.status == "OK" ?
                                                { color: "#21ac4d" }
                                                : { color: "#ef3340" }}>
                                            {disk.status == "OK" ? "✔" : "✘"}
                                        </span>
                                    </td>
                                    <td scope="col">
                                        <div class="progress">
                                            <div className={"progress-bar"}
                                                style={{width: Math.round(disk.diskspace_used*100/disk.diskspace_available)}}
                                                role="progressbar"
                                                aria-valuenow= {Math.round(disk.diskspace_used*100/disk.diskspace_available)}
                                                aria-valuemin="0"
                                                aria-valuemax="100"></div>
                                        </div>
                                    </td>
                                </tr>
                            )
                        )
                            : null
                        }


                    </tbody>
                </table>

            </div>
        )
    }
}
