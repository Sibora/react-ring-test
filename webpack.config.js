/**
 * @webpack.config.js 
 * Webpack configuration file.
 *
 * It takes ./src/js/app.js as the entry point to produce
 * an ouput into ./dist/js/main.js.
 * Plus, for every file with a .jsextension Webpack pipes 
 * the code through babel-loader for transforming ES6 down to ES5.
 * 
 * Webpacks needs two additional components for processing HTML:
 *  html-webpack-plugin and html-loader.
 * 
 */

const path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
  mode: "development",
  entry: ["./src/index.js"],
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js"
  },
  devServer: {
    contentBase: "./dist"
  },
  module: {
    rules: [
      {
        test: /\.(pdf|jpg|png|gif|svg|ico)$/,
        exclude: /node_modules/,
        use: [ 
            {
                loader: 'file-loader'
            },
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader"
          }
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    })
  ]
};